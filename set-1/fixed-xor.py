'''

Fixed XOR

Write a function that takes two equal-length buffers and produces their XOR combination.
If your function works properly, then when you feed it the string:

1c0111001f010100061a024b53535009181c

... after hex decoding, and when XOR'd against:

686974207468652062756c6c277320657965

... should produce:

746865206b696420646f6e277420706c6179

'''

import binascii
from Cryptodome.Util.strxor import strxor

a = "1c0111001f010100061a024b53535009181c"
b = "686974207468652062756c6c277320657965"
c = "746865206b696420646f6e277420706c6179"

def xor(input_1, input_2, control):
    unhex_1 = binascii.unhexlify(input_1)
    unhex_2 = binascii.unhexlify(input_2)
    control_unhex = binascii.unhexlify(control)
    result = strxor(unhex_1, unhex_2)
    if result != control_unhex:
        print("Error! Expected " + str(control) + ", \nresult: " + str(binascii.hexlify(result)))
    else:
        print("Success! Result: \n" + str(binascii.hexlify(result)))

xor(a, b, c)

exit()



