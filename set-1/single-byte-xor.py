'''

Single-byte XOR cipher

The hex encoded string:

1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736

... has been XOR'd against a single character. Find the key, decrypt the message.

'''

import codecs
import string

encoded_string = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"

for char in string.ascii_letters: # iterating through ASCII lowercase and uppercase letters
    decoded_string = codecs.decode(encoded_string, "hex").decode()
    message = "".join(chr(ord(hex_decoded_character) ^ ord(char)) for hex_decoded_character in decoded_string)
    if ' ' in message:
        print(message)

# Answer: 'Cooking MC's like a pound of bacon'