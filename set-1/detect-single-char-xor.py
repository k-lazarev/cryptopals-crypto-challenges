'''

Detect single-character XOR

One of the 60-character strings in detect-single-char-xor.txt has been encrypted by single-character XOR.

Find it.

'''

import codecs
import string

with open("detect-single-char-xor.txt", "r") as f:
    lines = [line.strip() for line in f]
f.close()

def find_message(l):
    for char in string.digits:
        try:
            decoded_string = codecs.decode(l, "hex").decode()
            message = "".join(chr(ord(hex_decoded_character) ^ ord(char)) for hex_decoded_character in decoded_string)
            if ' ' in message:
                print(message)
        except UnicodeDecodeError:
            pass

for l in lines:
    find_message(l)

# Answer 'Now that the party is jumping'